package ictgradschool.industry.lab08.ex02;

import ictgradschool.Keyboard;
import java.io.*;

import java.security.Key;

public class MyReader {

    public void start() {

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a BufferedReader.

        System.out.println("Please enter a file name: ");
        String fileName = Keyboard.readInput();

        File myfile = new File(fileName);
        System.out.println("Debug: your file name is: " + fileName);

        try (BufferedReader reader = new BufferedReader(new FileReader(myfile))) {

            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
            reader.close();

        }

        catch (IOException e) {
            System.out.println("Error");
        }

    }

    public static void main(String[] args) {
        new MyReader().start();
    }
}
