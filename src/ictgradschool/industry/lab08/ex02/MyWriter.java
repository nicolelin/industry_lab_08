package ictgradschool.industry.lab08.ex02;

import ictgradschool.Keyboard;
import javafx.print.Printer;
import java.io.IOException;

import java.io.*;

/**
 * A simple program which should allow the user to type any number of text lines. The program will then
 * write them out to a file.
 */
public class MyWriter {

    public void start() {

        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();
        System.out.println("Debug: your file name is: " + fileName);

        // TODO Open a file for writing, using a PrintWriter.

        File myfile = new File(fileName);

        try {
            PrintWriter writer = new PrintWriter(new FileWriter(myfile));

            while (true) {

                System.out.print("Type a line of text, or just press ENTER to quit: ");
                String text = Keyboard.readInput();

                if (text.isEmpty()) {
                    break;
                }

                // TODO Write the user's line of text to a file.
                writer.write(text);
                System.out.println("You wrote: " + text);

            }

            System.out.println("Done!");
            writer.close();

        } catch (IOException e) {
            System.out.println("Error");
        }
    }

    public static void main(String[] args) {

        new MyWriter().start();

    }
}
