package ictgradschool.industry.lab08.ex02;

import ictgradschool.Keyboard;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class MyScanner {

    public void start() {

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a Scanner.

        System.out.println("Please enter a file name: ");
        String fileName = Keyboard.readInput();

        File myfile = new File(fileName);
        System.out.println("Debug: your file name is: " + fileName);

        try (Scanner scanner = new Scanner(myfile)) {

            while (scanner.hasNextLine()) {
                System.out.println(scanner.nextLine());
            }
            scanner.close();

        } catch (IOException e) {
            System.out.println("Error");
        }

    }

    public static void main(String[] args) {
        new MyScanner().start();
    }
}
