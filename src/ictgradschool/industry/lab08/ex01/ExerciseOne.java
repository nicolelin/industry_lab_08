package ictgradschool.industry.lab08.ex01;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ExerciseOne {

    public void start() {

        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }

    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.

        FileReader input = null;

        try {
            input = new FileReader("input2.txt");
            numE = input.read();
            total = input.read();
            input.close();
        }
        catch (IOException e) {
            System.out.println("Error");
        }

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.

        File myFile = new File("input2.txt");

        try {
            BufferedReader reader = new BufferedReader(new FileReader(myFile));
            numE = reader.read();
            total = reader.read();
            reader.close();
        }
        catch (IOException e) {
            System.out.println("Error");
        }

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}
