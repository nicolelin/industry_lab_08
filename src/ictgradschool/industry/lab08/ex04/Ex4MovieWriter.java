package ictgradschool.industry.lab08.ex04;

import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieWriter;

import java.io.*;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieWriter extends MovieWriter {

    @Override
    protected void saveMovies(String fileName, Movie[] films) {

        // TODO Implement this with a PrintWriter

        File movieFile = new File(fileName);

        try (PrintWriter writer = new PrintWriter(new FileWriter(movieFile))) {
            System.out.println("Note: you are now saving CSV to file: " + movieFile); // Debug

            writer.write(films.length+ "\n");

            for (int i = 0; i < films.length; i++) {

//                writer.write(films[i].getName());
//                writer.write(films[i].getYear());
//                writer.write(films[i].getLengthInMinutes());
//                writer.write(films[i].getDirector());

                writer.write(films[i].getName() + "," + films[i].getYear() + "," + films[i].getLengthInMinutes() + "," + films[i].getDirector() + "\n");

                System.out.println("[Movie added:] " + i); // Debug
            }

            writer.close();

        }

        catch (IOException e) {
            System.out.println("Error writing to file.");
        }


        System.out.println("Movies saved successfully to " + fileName + "!");

    }

    public static void main(String[] args) {
        new Ex4MovieWriter().start();
    }

}
