package ictgradschool.industry.lab08.ex04;

import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {

        // TODO Implement this with a Scanner

        File movieFile = new File(fileName);

        int i = 0;
        Movie[] films;

        try (Scanner scanner = new Scanner(movieFile)) {

            System.out.println("Note: you are now reading file: " + movieFile); // Debug

            int arrayLength = scanner.nextInt();
            films = new Movie[arrayLength];

            scanner.useDelimiter(",|(\\r\\n|\\n|\\r)");

            while (scanner.hasNext()) {
                String name = scanner.next();
                int year = scanner.nextInt();
                int lengthInMinutes = scanner.nextInt();
                String director = scanner.next();

                Movie movie = new Movie(name, year, lengthInMinutes, director);
                films[i] = movie;
                i++;

                System.out.println("Loaded: " + name + " " + year + " " + lengthInMinutes + " " + director); // Debug
            }

            System.out.println();

            System.out.println("Movies loaded successfully from " + fileName + "!");

            System.out.println();

            scanner.close();

            return films;

        } catch (IOException e) {
            System.out.println("Error reading from file.");
        }
        System.out.println("Movies not loaded successfully from " + fileName + "!");
        return null;
    }

    public static void main(String[] args) {
        new Ex4MovieReader().start();
    }
}
